C / Fortran IRL Interface
================

IRL, while written in C++14, contains a C and Fortran2008 interface. Documentating this interface is currently ongoing, but some examples do exist in the `examples/fortran` directory.
It should also be noted, while not thoroughly documented, the Fortran interface has already been used in several large scale CFD flow solvers and is capable of delivering the majority of IRL's functionality.
