// This file is part of the Interface Reconstruction Library (IRL),
// a library for interface reconstruction and computational geometry operations.
//
// Copyright (C) 2019 Robert Chiodi <robert.chiodi@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SRC_OPTIMIZATION_OPTIMIZERS_H_
#define SRC_OPTIMIZATION_OPTIMIZERS_H_

#include "src/optimization/bfgs.h"
#include "src/optimization/bisection.h"
#include "src/optimization/brents_method.h"
#include "src/optimization/illinois.h"
#include "src/optimization/levenberg_marquardt.h"
#include "src/optimization/optimizers.h"
#include "src/optimization/secant.h"

#endif  // SRC_OPTIMIZATION_OPTIMIZERS_H_
